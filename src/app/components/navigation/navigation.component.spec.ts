import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { NavigationComponent } from './navigation.component'
import { AppComponent } from 'src/app/app.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

describe('NavigationComponent', () => {
  let component: NavigationComponent
  let fixture: ComponentFixture<NavigationComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [NavigationComponent, AppComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  xit('should create', () => {
    expect(component).toBeTruthy()
  })
})
