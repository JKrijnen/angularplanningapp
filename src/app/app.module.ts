import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NavigationComponent } from './components/navigation/navigation.component'
import { AboutComponent } from './components/about/about.component'

@NgModule({
  declarations: [AppComponent, NavigationComponent, AboutComponent],
  imports: [BrowserModule, AppRoutingModule, NgbModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
